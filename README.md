# helloworld-ts

This is a simple example of a JavaScript project that uses express to serve a simple web page.

## Getting Started

### Prerequisites

You need to have [Node.js](https://nodejs.org/en/) installed.

### Installing

Clone the repository and install the dependencies:

```bash
git clone https://gitlab.unige.ch/oct_cui/helloworld-js.git
cd helloworld-js
npm install
```

### Running

To run the project, start the server:

```bash
npm start
```

You can now access the web page at http://localhost:3000.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
